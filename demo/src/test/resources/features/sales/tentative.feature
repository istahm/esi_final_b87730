Feature: Tentative reservation request


  Scenario: 1.  tentative reservation request rejected because submitted too late
    When the customer queries the plant catalog for tentative reservation
    Then response shouldbe conflict

  Scenario:  2 .tentative reservation request rejected because plant inventory item is not aviable for the whole period
    When the customer queries the plant catalog for tentative reservation for case second test
    Then response shouldbe conflict for that case

  Scenario:  3 tentative reservation request accepted and after  other same tentative reservation should be rejected
    When the customer queries the plant catalog for tentative reservation for case third test reject
    Then response shouldbe conflict for that case third test

  Scenario: 4 .tentative reservation request accepted and then pre-reserved PO referring this tentative reservation
    When the customer queries the plant catalog for tentative reservation for case fourth  test
    Then response shouldbe OK for this case