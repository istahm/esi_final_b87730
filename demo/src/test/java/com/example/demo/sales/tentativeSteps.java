package com.example.demo.sales;

import com.example.demo.DemoApplication;
import com.example.demo.inventory.domain.model.EquipmentCondition;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlDateInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@ContextConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class tentativeSteps {

    @Autowired
    private WebApplicationContext wac;

    private WebClient customerBrowser;
    HtmlPage customerPage;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Before  // Use `Before` from Cucumber library
    public void setUp() {
        customerBrowser = MockMvcWebClientBuilder.webAppContextSetup(wac).build();
    }

    @After  // Use `After` from Cucumber library
    public void tearOff() {
        purchaseOrderRepository.deleteAll();
        plantReservationRepository.deleteAll();
        plantInventoryItemRepository.deleteAll();
        plantInventoryEntryRepository.deleteAll();
    }

    @When("^the customer queries the plant catalog for tentative reservation$")
    @Sql("/plants-dataset.sql")
    public void the_customer_queries_the_plant_catalog_for_tentative_reservation() throws Throwable {
        put("/api/sales/orders/1/")
                .content("{\"_id\":1,\"rentalPeriod\":{\"endDate\":\"2019-09-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":1, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;
    }

    @Then("^response shouldbe conflict$")
    @Sql("/plants-dataset.sql")
    public void response_shouldbe_conflict() throws Throwable {
        MockHttpServletRequestBuilder result =
                put("/api/sales/orders/1/")
                        .content("{\"_id\":1,\"rentalPeriod\":{\"endDate\":\"2019-09-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":1, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;



        assertThat(result.equals(409));
    }

    @When("^the customer queries the plant catalog for tentative reservation for case second test$")
    @Sql("/plants-dataset.sql")
    public void the_customer_queries_the_plant_catalog_for_tentative_reservation_for_case_second_test() throws Throwable {
        put("/api/sales/orders/1/")
                .content("{\"_id\":888,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":888, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;

    }

    @Then("^response shouldbe conflict for that case$")
    @Sql("/plants-dataset.sql")
    public void response_shouldbe_conflict_for_that_case() throws Throwable {
        MockHttpServletRequestBuilder result =
                put("/api/sales/orders/1/")
                        .content("{\"_id\":888,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":888, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;



        assertThat(result.equals(409));
    }

    @When("^the customer queries the plant catalog for tentative reservation for case third test reject$")
    @Sql("/plants-dataset.sql")
    public void the_customer_queries_the_plant_catalog_for_tentative_reservation_for_case_third_test_reject() throws Throwable {
        put("/api/sales/orders/2/")
                .content("{\"_id\":2,\"rentalPeriod\":{\"endDate\":\"2019-09-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":2, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;

        put("/api/sales/orders/2/")
                .content("{\"_id\":2,\"rentalPeriod\":{\"endDate\":\"2019-09-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":2, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;
    }

    @Then("^response shouldbe conflict for that case third test$")
    @Sql("/plants-dataset.sql")
    public void response_shouldbe_conflict_for_that_case_third_test() throws Throwable {
        MockHttpServletRequestBuilder result =
                put("/api/sales/orders/2/")
                        .content("{\"_id\":2,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":2, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;



        assertThat(result.equals(200));
        MockHttpServletRequestBuilder result1 =
                put("/api/sales/orders/2/")
                        .content("{\"_id\":2,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":2, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;



        assertThat(result1.equals(409));

    }

    @When("^the customer queries the plant catalog for tentative reservation for case fourth  test$")
    @Sql("/plants-dataset.sql")
    public void the_customer_queries_the_plant_catalog_for_tentative_reservation_for_case_fourth_test() throws Throwable {
        put("/api/sales/orders/3/")
                .content("{\"_id\":3,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":3, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;

        put("/api/sales/orders/3/")
                .content("{\"_id\":3,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":3, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;

    }

    @Then("^response shouldbe OK for this case$")
    @Sql("/plants-dataset.sql")
    public void response_shouldbe_OK_for_this_case() throws Throwable {
        MockHttpServletRequestBuilder result =
                put("/api/sales/orders/3/")
                        .content("{\"_id\":3,\"rentalPeriod\":{\"endDate\":\"2019-10-22\",\"startDate\":\"2019-06-25\"},\"status\":\"PENDING\",\"plant\":{\"_id\":3, \"name\": \"excavador\", \"description\": \"old\", \"price\": 800}") ;



        assertThat(result.equals(200));
        MockHttpServletRequestBuilder result2 =
                put("/api/sales/pre-reserved/3/")
                        .content("{\"_id\":3,\"serial_number\": ABB209391}") ;



        assertThat(result2.equals(200));
    }


}
